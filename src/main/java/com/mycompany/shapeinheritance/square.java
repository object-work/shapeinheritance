/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeinheritance;

/**
 *
 * @author 66955
 */
public class square extends rectangle{
    
    private double s;
    public square (double s){
        super (s,s);
        this.s = s;
    } 
    public double calArea(){
        return s*s;
    }
    
    public void print(){
        System.out.println("square side = "+this.s + " Area = "+calArea());
    }
        
    
}
