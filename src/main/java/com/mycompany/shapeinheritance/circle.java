/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeinheritance;

/**
 *
 * @author 66955
 */
public class circle extends shape{
    private double r;
    static final double pi = 22.0/7;
    
    public circle(double r){
        this.r = r;
    }
    
    public double calArea(){
        return pi * r * r;
    }
    
    public void print(){
        System.out.println("Circle = "+this.r +" Area = " +calArea());
    }  
}
