/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeinheritance;

/**
 *
 * @author 66955
 */
public class testshape {
    public static void main(String[] args){
        shape shape = new shape();
        shape.calArea();
        shape.print();
        
        circle circle= new circle(2);
        circle.calArea();
        circle.print();
        
        triangle triangle = new triangle(2,4);
        triangle.calArea();
        triangle.print();
        
        rectangle rectangle = new rectangle(4,3);
        rectangle.calArea();
        rectangle.print();
        
        square square = new square(5);
        square.calArea();
        square.print();
        
        System.out.println("circle is shape: "+(circle instanceof shape));
        System.out.println("triangle is shape: "+(triangle instanceof shape));
        System.out.println("rectangle is shape: "+(rectangle instanceof shape));
        System.out.println("square is shape: "+(square instanceof shape));
        System.out.println("shape is shape: "+(shape instanceof shape));
        
        shape[] Shape = {circle,triangle,rectangle,square};
        for(int i = 0;i<Shape.length;i++){
            Shape[i].print();
        }

    }
}
