/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeinheritance;

/**
 *
 * @author 66955
 */
public class triangle extends shape{
    private double height;
    private double base;
    
    public triangle (double height,double base){
        this.height = height;
        this.base = base;
    }
    public double calArea(){
        return (height*base)/2;
    }
    
    public void print(){
        System.out.println("Triangle base = "+this.base+" height = "+this.height +" area = " +calArea());
    }  
}
