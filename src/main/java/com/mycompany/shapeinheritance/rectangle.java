/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeinheritance;

/**
 *
 * @author 66955
 */
public class rectangle extends shape{
    private double width;
    private double height;
    
    public rectangle (double width,double height){
        this.width = width;
        this.height = height;
    }
    
    public double calArea(){
        return width*height;
    }
    
    public void print(){
        System.out.println("rectangle width = "+this.width +" height = "+this.height +" area = " +calArea());
    }  
}
